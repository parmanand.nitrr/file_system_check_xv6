/*****************************************************************************/
/*
                        File System Checker
*/
/*****************************************************************************/    
/************************** Header Files *************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <assert.h>
#include <stdbool.h>

/*******************************************************************************/

/************************ Declarations from types.h ****************************/

typedef unsigned int   uint;
typedef unsigned short ushort;
typedef unsigned char  uchar;
typedef uint pde_t;

/*******************************************************************************/

/************************** Declarations from fs.h *****************************/

/*
    On-disk file system format.
    Block 0 is unused.
    Block 1 is super block.
    Inodes start at block 2.
*/

#define ROOTINO 1  // root i-number
#define BSIZE 512  // block size

// File system super block
struct superblock {
  uint size;         // Size of file system image (blocks)
  uint nblocks;      // Number of data blocks
  uint ninodes;      // Number of inodes.
};

#define NDIRECT 12
#define NINDIRECT (BSIZE / sizeof(uint))
#define MAXFILE (NDIRECT + NINDIRECT)

// On-disk inode structure
struct dinode {
  short type;           // File type
  short major;          // Major device number (T_DEV only)
  short minor;          // Minor device number (T_DEV only)
  short nlink;          // Number of links to inode in file system
  uint size;            // Size of file (bytes)
  uint addrs[NDIRECT+1];   // Data block addresses
};

// Inodes per block.
#define IPB           (BSIZE / sizeof(struct dinode))

// Block containing inode i
#define IBLOCK(i)     ((i) / IPB + 2)

// Bitmap bits per block
#define BPB           (BSIZE*8)

// Block containing bit for block b
#define BBLOCK(b, ninodes) (b/BPB + (ninodes)/IPB + 3)

// Directory is a file containing a sequence of dirent structures.
#define DIRSIZ 14

struct dirent {
  ushort inum;
  char name[DIRSIZ];
};

/*******************************************************************************/

/*************************** Global Declarations *******************************/

#define T_DIR       1  
#define T_FILE      2  
#define T_DEV       3  

#define BSIZE 512
#define BLOCK_SIZE (BSIZE)

#define PERROR(msg...) fprintf(stderr, msg)
char bitarr[8] = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };
#define BITSET(bitmap_block_start, blockaddr) ((*(bitmap_block_start + blockaddr / 8)) & (bitarr[blockaddr % 8]))

#define DPB (BSIZE / sizeof(struct dirent))

typedef struct _fs {
    uint inode_blocks_num;
    uint bitmap_blocks_num;
    uint datablock_start;
    struct superblock *sb;
    char *inode_block_start;
    char *bitmap_block_start;
    char *datablocks;
    char *img_ptr;
} fs;

/*******************************************************************************/

/***************************** Check 1:Bad Inode *******************************/

int bad_inode(struct dinode *inode){
    if(inode->type == T_DIR || inode->type == T_FILE || inode->type == T_DEV){
        return 0;
    }
    return 1;
}

/*******************************************************************************/

/************************** Check 2 : bad direct address ***********************/

int bad_direct_addr(fs *filestats, struct dinode *inode){
    
    int x;
    uint blockaddr;
    for (x = 0; x < NDIRECT; x++) {
        blockaddr = inode->addrs[x];
        if (blockaddr == 0){
            continue;
        }
        if (blockaddr < 0 || blockaddr >= filestats->sb->size){
            return 1;
        }
    }
    return 0;
}

/*******************************************************************************/

/************************ Check 2 : bad indirect address ***********************/

int bad_indir_addr(fs *filestats, struct dinode *inode){
    
    uint blockaddr;
    blockaddr = inode->addrs[NDIRECT];
    uint *indirect_ptr;
    int x;

    if (blockaddr == 0){
        return 0;
    }

    if (blockaddr < 0 || blockaddr >= filestats->sb->size) {
        return 1;
    }

    indirect_ptr = (uint *) (filestats->img_ptr + blockaddr * BSIZE);
    for (x = 0; x < NINDIRECT; x++, indirect_ptr++) {
        blockaddr = *(indirect_ptr);
        if (blockaddr == 0)
            continue;
       
        if (blockaddr < 0 || blockaddr >= filestats->sb->size) {
            return 1;
        }
    }
    return 0;
}

/*******************************************************************************/

/**************************** Check Directory format ***************************/

int directory_fmt(fs *filestats, struct dinode *inode, int inum) {
    
    int x, y, parent=0, self=0;
    uint blockaddr;
    struct dirent *de;
   
    for (x = 0; x < NDIRECT; x++){
        blockaddr = inode->addrs[x];
        if (blockaddr == 0){
            continue;
        }

        de = (struct dirent *) (filestats->img_ptr + blockaddr * BSIZE);
        for (y = 0; y < DPB; y++, de++) {
            if (!self && strcmp(".", de->name) == 0){
                self = 1;
                if (de->inum != inum){
                    return 1;
                }
            }
            if (!parent && strcmp("..", de->name) == 0){
                parent = 1;

                if (inum != 1 && de->inum == inum){
                    return 1;
                }
                if (inum == 1 && de->inum != inum){
                    return 1;
                }
            }

            if (parent && self){
                break;
            }
        }

        if (parent && self){
            break;
        }
    }

    if (!parent || !self){
        return 1;
    }

    return 0;
}

/*******************************************************************************/

/*******************************************************************************/

int bmap(fs *filestats, struct dinode *inode){
    
    int x, y;
    uint blkaddr;
    uint *indr;

    for (x = 0; x < (NDIRECT + 1); x++){    // Looping over Direct addresses
        blkaddr = inode->addrs[x];
        if (blkaddr == 0){
            continue;
        }

        if (!BITSET(filestats->bitmap_block_start, blkaddr)){
            return 1;
        }
       
        if (x == NDIRECT){
            indr = (uint *) (filestats->img_ptr + blkaddr * BSIZE);
            for (y = 0; y < NINDIRECT; y++, indr++){    // Looping over indirect addresses
                blkaddr = *(indr);
                if (blkaddr == 0){
                    continue;
                }

                if (!BITSET(filestats->bitmap_block_start, blkaddr)){
                    return 1;
                }
            } // End of for
        }
    }   // End of for
    return 0;
}

/*******************************************************************************/

/****************************** Check 1,2,3,4,5 ********************************/
/*
    1 : bad inode.
    2 : bad direct address in inode, bad indirect address in inode.
    3 : root directory does not exist.
    4 : directory not properly formatted.
    5 : address used by inode but marked free in bitmap.
*/
/*******************************************************************************/

int in_use_inode_chk(fs *filestats){
    
    struct dinode *inode;
    int x, unalloc = 0;
    inode = (struct dinode *)(filestats->inode_block_start);  // inode start

    for(x = 0; x < filestats->sb->ninodes; x++, inode++){

        if (inode->type == 0){
            unalloc++;
            continue;
        }

        // Check 1
        if (bad_inode(inode) != 0){
            PERROR("ERROR: bad inode.\n");
            return 1;
        }

        // Check 2
        if (bad_direct_addr(filestats, inode) != 0){
            PERROR("ERROR: bad direct address in inode.\n");
            return 1;
        }

        // Check 2
        if (bad_indir_addr(filestats, inode) != 0){
            PERROR("ERROR: bad indirect address in inode.\n");
            return 1;
        }

        // Check 3
        if (x == 1 && (inode->type != T_DIR || directory_fmt(filestats, inode, x) != 0)){
            PERROR("ERROR: root directory does not exist.\n");
            return 1;
        }

        // Check 4
        if (inode->type == T_DIR && directory_fmt(filestats, inode, x) != 0){
            PERROR("ERROR: directory not properly formatted.\n");
            return 1;
        }

        // Check 5
        if (bmap(filestats, inode) != 0){
            PERROR("ERROR: address used by inode but marked free in bitmap.\n");
            return 1;
        }
    }
    return 0;
}

/*******************************************************************************/

/********************************** Check 6 ************************************/
/*
    6 : bitmap marks block in use but it is not in use.
*/
/*******************************************************************************/

int bmap_check(fs *filestats){
    
    struct dinode *inode;
    int i,j,k;
    int dblcks[filestats->sb->nblocks];
    uint blockaddr;
    uint *indirect;
    memset(dblcks, 0, filestats->sb->nblocks * sizeof(int));

    inode = (struct dinode *)(filestats->inode_block_start);  // Inode start

    for(i = 0; i < filestats->sb->ninodes; i++, inode++){
        if (inode->type == 0){
            continue;
        }  
        for (k = 0; k < (NDIRECT + 1); k++){    // Looping over direct blocks
            blockaddr = inode->addrs[k];
            if (blockaddr == 0){
                continue;
            }

            dblcks[blockaddr - filestats->datablock_start] = 1;

            if (k == NDIRECT){
                indirect = (uint *) (filestats->img_ptr + blockaddr * BSIZE);
                for (j = 0; j < NINDIRECT; j++, indirect++){    // Looping over indirect blocks
                    blockaddr = *(indirect);
                    if (blockaddr == 0){
                        continue;
                    }

                    dblcks[blockaddr - filestats->datablock_start] = 1;
                }   // End of for
            }
        }   // End of for
    }   // End of for
   
    for (i = 0; i < filestats->sb->nblocks; i++){
        blockaddr = (uint) (i + filestats->datablock_start);
        if (dblcks[i] == 0 && BITSET(filestats->bitmap_block_start, blockaddr))   // If bit at i is not same as bit at ondisk bitmap
        {
            PERROR("ERROR: bitmap marks block in use but it is not in use.\n");
            return 1;
        }
    }
    return 0;
}

/*******************************************************************************/

/********************************* Check 7, 8 **********************************/
/*
    7 : direct address used more than once.
    8 : indirect address used more than once.
*/
/*******************************************************************************/

int addrs_check(fs *filestats){
    
    struct dinode *inode;
    int i,j,k;
    uint blockaddr;
    uint *indirect;
    uint dbaddr[filestats->sb->nblocks];

    memset(dbaddr, 0, sizeof(uint) * filestats->sb->nblocks);

    uint iuaddrs[filestats->sb->nblocks];
    memset(iuaddrs, 0, sizeof(uint) * filestats->sb->nblocks);

    inode = (struct dinode *)(filestats->inode_block_start);  // inode start
   
    for(i = 0; i < filestats->sb->ninodes; i++, inode++){
        if (inode->type == 0){
            continue;
        }

        for (j = 0; j < NDIRECT; j++){  // Looping over direct addresses
            blockaddr = inode->addrs[j];
            if (blockaddr == 0){
                continue;
            }
            dbaddr[blockaddr - filestats->datablock_start]++;   // Updating data bitmap
        }

        blockaddr = inode->addrs[NDIRECT];
        indirect = (uint *) (filestats->img_ptr + blockaddr * BSIZE);
        for (k = 0; k < NINDIRECT; k++, indirect++){    // Looping over indirect blocks
            blockaddr = *(indirect);    
            if (blockaddr == 0){
                continue;
            }

            iuaddrs[blockaddr - filestats->datablock_start]++;   // Updating data bitmap
        }
    }

    for (i = 0; i < filestats->sb->nblocks; i++) {
        // Check 7
        if (dbaddr[i] > 1) {
            PERROR("ERROR: direct address used more than once.\n");
            return 1;
        }

        // Check 8
        if (iuaddrs[i] > 1) {
            PERROR("ERROR: indirect address used more than once.\n");
            return 1;
        }
    }
    return 0;
}

/*******************************************************************************/

/****************** Recursively traverse the directory *************************/

void directory_traversal(fs *filestats, struct dinode *rootinode, int *inodemap){
    
    int i, j;
    uint blockaddr;
    uint *indirect;
    struct dinode *inode;
    struct dirent *dir;
   
    if (rootinode->type == T_DIR) {
        for (i = 0; i < NDIRECT; i++) { // Looping over the Direct addresses
            blockaddr = rootinode->addrs[i];
            if (blockaddr == 0)
                continue;

            dir = (struct dirent *) (filestats->img_ptr + blockaddr * BSIZE); // Directory entry
            for (j = 0; j < DPB; j++, dir++) {
                if (dir->inum != 0 && strcmp(dir->name, ".") != 0 && strcmp(dir->name, "..") != 0) {
                    inode = ((struct dinode *) (filestats->inode_block_start)) + dir->inum;
                    inodemap[dir->inum]++;  // Increase the value at inode no = inum by one
                    directory_traversal(filestats, inode, inodemap);    // Recusrive call
                }
            } // end of for
        } // end of for

        blockaddr = rootinode->addrs[NDIRECT];
        if (blockaddr != 0) {
            indirect = (uint *) (filestats->img_ptr + blockaddr * BSIZE); // Indirect block address
            for (i = 0; i < NINDIRECT; i++, indirect++) {   // Looping over Indirect addresses
                blockaddr = *(indirect);
                if (blockaddr == 0)
                    continue;

                dir = (struct dirent *) (filestats->img_ptr + blockaddr * BSIZE); // Directory entry

                for (j = 0; j < DPB; j++, dir++) {
                    if (dir->inum != 0 && strcmp(dir->name, ".") != 0 && strcmp(dir->name, "..") != 0) {
                        inode = ((struct dinode *) (filestats->inode_block_start)) + dir->inum;
                        inodemap[dir->inum]++;  // Increase the value at inode no = inum by one
                        directory_traversal(filestats, inode, inodemap);    // recursive call 
                    }
                } // end of for
            } // end of for
        }
    }
}

/*******************************************************************************/

/***************************** Check 9, 10, 11, 12 *****************************/
/*
    9 : inode marked use but not found in a directory.
    10 : inode referred to in directory but marked free.
    11 : bad reference count for file.
    12 : directory appears more than once in file system.
*/
/*******************************************************************************/

int check_dir(fs *filestats){
    
    int i;
    int inodemap[filestats->sb->ninodes];                       // Inode bitmap
    memset(inodemap, 0, sizeof(int) * filestats->sb->ninodes);
    struct dinode *inode, *rootinode;

    inode = (struct dinode *) (filestats->inode_block_start);         // inode start
    rootinode = ++inode;                                        // root inode
   
    inodemap[0]++;
    inodemap[1]++;
   
    directory_traversal(filestats, rootinode, inodemap);    // Calling the recursive function to traverse directory

    inode++;
    
    for (i = 2; i < filestats->sb->ninodes; i++, inode++) {
        // Check 9
        if (inode->type != 0 && inodemap[i] == 0) {
            PERROR("ERROR: inode marked use but not found in a directory.\n");
            return 1;
        }

        // Check 10
        if (inodemap[i] > 0 && inode->type == 0) {
            PERROR("ERROR: inode referred to in directory but marked free.\n");
            return 1;
        }

        // Check 11
        if (inode->type == T_FILE && inode->nlink != inodemap[i]) {
            PERROR("ERROR: bad reference count for file.\n");
            return 1;
        }

        // Check 12
        if (inode->type == T_DIR && inodemap[i] > 1) {
            PERROR("ERROR: directory appears more than once in file system.\n");
            return 1;
        }
    }
    return 0;
}

/*******************************************************************************/

/******************************* Main Method ***********************************/

int main(int argc, char *argv[])
{
  int fsfd;
  char *addr;
  struct stat status;
  fs fsinfo;

  if(argc < 2){
    fprintf(stderr, "Usage: fcheck <file_system_image>\n"); // Wrongly input arguments
    exit(1);
  }


  fsfd = open(argv[1], O_RDONLY);
  if(fsfd < 0)
  {
    fprintf(stderr, "image not found.\n");                  // Image not found
    exit(1);
  }
 
  if (fstat(fsfd, &status) != 0){
    fprintf(stderr,"failed to fstat file %s \n", argv[1]);
    return 1;
  }

  /* Dont hard code the size of file. Use fstat to get the size */
  addr = mmap(NULL, status.st_size, PROT_READ, MAP_PRIVATE, fsfd, 0);
  if (addr == MAP_FAILED){
    fprintf(stderr,"mmap failed.\n");
    exit(1);
  }

  /* read the super block */
  fsinfo.img_ptr = addr;                                                                            // Image pointer
  fsinfo.sb = (struct superblock *) (addr + 1 * BLOCK_SIZE);                                        // Superblock start
  fsinfo.inode_blocks_num = (fsinfo.sb->ninodes / (IPB)) + 1;                                       // Number of inode blocks
  fsinfo.bitmap_blocks_num =  (fsinfo.sb->size / (BPB)) + 1;                                        // Number of bitmap blocks
  fsinfo.inode_block_start = (char *) (addr + BSIZE * 2);                                           // Inode block start
  fsinfo.bitmap_block_start = (char *) (fsinfo.inode_block_start + BSIZE * fsinfo.inode_blocks_num);// Bitmap block start
  fsinfo.datablocks = (char *) (fsinfo.bitmap_block_start + BSIZE * fsinfo.bitmap_blocks_num);      // Data blocks 
  fsinfo.datablock_start = fsinfo.inode_blocks_num + fsinfo.bitmap_blocks_num + 2;                  // Data block start
 
  /* Memory Cleanup */
  int temp;
  temp = in_use_inode_chk(&fsinfo);
    if (temp != 0){
        goto cleanup;
    }

    temp = bmap_check(&fsinfo);
    if (temp != 0){
        goto cleanup;
    }

    temp = addrs_check(&fsinfo);
    if (temp != 0){
        goto cleanup;
    }

    temp = check_dir(&fsinfo);
    if (temp != 0){
        goto cleanup;
    }
    cleanup:
    munmap(addr, status.st_size);
    close(fsfd);

  exit(0); // exit successfully

}

/******************************** End of Main **********************************/

/******************************** End of Code **********************************/