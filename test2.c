#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include "fs.h"
#include "types.h"

char *img_ptr;
struct superblock *sb;
struct dinode *dip;
char *bitmap;
struct dirent *dir;
unsigned long on_disk_bmap[BSIZE*8];
int direct_addrs[BSIZE];
int indirect_addrs[BSIZE*2];

//Check 1
void check_bad_inode(){

    // Each inode is either unallocated = 0, or of one of the types 1:directory or 2:file, Check 1, status: ?
    struct dinode *cur_dinode = (struct dinode *)(img_ptr + IBLOCK((uint)0)*BSIZE);  // inode block start
    for(int i=1; i <= sb->ninodes; i++){
      if(cur_dinode->type != 0 && cur_dinode->type != 1 && cur_dinode->type != 2 && cur_dinode->type != 3){
        fprintf(stderr, "ERROR: bad inode.\n");
        exit(1);
      }
      cur_dinode++;
    }
}

//Check 2
void check_bad_direct_and_indirect(){

  struct dinode *cur_dinode = (struct dinode *)(img_ptr + IBLOCK((uint)0)*BSIZE);  // inode block start
  uint *indirect_ptr;
  int index_direct = 0;
  int index_indirect = 0;

  for(int i=1; i <= sb->ninodes; i++){

    if(cur_dinode->type == 1 || cur_dinode->type == 2 || cur_dinode->type == 3){

    // Bad direct address in inode, Check 2, status: ?
    for(int j=0; j < NDIRECT; j++){

      for(int x = 0; x < index_direct; x++){
	      //printf("direct address at %d is %d\n", x, direct_addrs[x]);
        if (direct_addrs[x] != 0 && direct_addrs[x] == cur_dinode[i].addrs[j]){	
          fprintf(stderr, "ERROR: direct address used more than once.\n");
          exit(1);
        }
      }
	
      if(cur_dinode[i].addrs[j] != 0){	
      	direct_addrs[index_direct] = cur_dinode[i].addrs[j];
  	    index_direct++;
	      //printf("index_direct %d\n",index_direct);
      }
  
      // printf("counter %d direct address%d\n",j,cur_dinode[i].addrs[j]);
      if(cur_dinode[i].addrs[j] < 0 || cur_dinode[i].addrs[j] > sb->size){
        fprintf(stderr, "ERROR: bad direct address in node.\n");
        exit(1);
      }	
    } // End of for loop

    if (cur_dinode[i].addrs[NDIRECT] != 0){

      // Bad indirect address in inode, Check 2, status: ?
      indirect_ptr = (uint *)(img_ptr + (cur_dinode[i].addrs[NDIRECT])*BSIZE);
      for(int k=0; k < NINDIRECT; k++){

        for(int y = 0; y < index_indirect; y++){
	        //printf("indirect address at %d is %d\n", y, indirect_addrs[y]);
          if (indirect_addrs[y] != 0 && indirect_addrs[y] == *indirect_ptr){
            fprintf(stderr, "ERROR: indirect address used more than once.\n");
            exit(1);
          }
        }
        
	if(*indirect_ptr != 0){
          indirect_addrs[index_indirect] = *indirect_ptr;
	  index_indirect++;
	  //printf("index_direct %d\n",index_indirect);
	}
        // printf("indirect %u\n",*indirect_ptr);
        if(*indirect_ptr < 0 || *indirect_ptr > sb->size){
          fprintf(stderr, "ERROR: bad indirect address in node.\n");
          exit(1);
        }
        indirect_ptr++;
      } //End of for loop
    } 
  }
  cur_dinode++;
  } // End of for loop

  for(int z=0;z<(sizeof(direct_addrs)/sizeof(direct_addrs[0]));z++){
      printf("direct-> %d\n",direct_addrs[z]);
  }
  for(int z=0;z<(sizeof(indirect_addrs)/sizeof(indirect_addrs[0]));z++){
      printf("indirect-> %d\n",indirect_addrs[z]);
  }

}

// Check 3
void check_root_dir_exist(){

  // Root directory inode and its parent inode == 1, Check 3, status: pass!
  if(!(dir) || dir->inum != 1 || (dir+1)->inum != 1 || dip[ROOTINO].type != 1){
    fprintf(stderr, "ERROR: root directory does not exist.\n");
    exit(1);
  }
}

//Bitmap generation
void traverse_bit_map(){
  int k;
  int bitindex = 0;
  for(k=0;k<512;k++){
    long unsigned i=0;
    long unsigned val = *bitmap; 
    while(i<8){
      long unsigned bit = val & (1<<i);
      i++;
      if(bit == 0){
        on_disk_bmap[bitindex]=0;
      } else {
          on_disk_bmap[bitindex]=1;
      }
      bitindex++;
    }
    bitmap++;
  }
  /*for(int z=0;z<(sizeof(on_disk_bmap)/sizeof(on_disk_bmap[0]));z++){
      printf("%d -> %lu \n",z,on_disk_bmap[z]);
  }*/
}

// Check 12
// No extra links in directories, Check 12, status: ?
void check_more_link_dir(){

  struct dinode *curr_dinode = (struct dinode *)(img_ptr + IBLOCK((uint)0)*BSIZE);  // inode block start
  curr_dinode += 2;
  for(int i=2; i < sb->ninodes; i++){
    if(curr_dinode->type == 1 && curr_dinode->nlink > 1){
      fprintf(stderr, "ERROR: directory appears more than once in file system.\n");
      exit(1);
    }
    curr_dinode++;
  }
}

int main(int argc, char const *argv[]) {

  if(argc < 2){
    fprintf(stderr, "Usage: fcheck <file_system_image>\n");
    exit(1);
  }

  int fd = open(argv[1], O_RDWR);
  if (fd < 0) {
    fprintf(stderr, "ERROR: image not found.\n");
    exit(1);
  }

  struct stat buf;
  fstat(fd, &buf);
  img_ptr = mmap(NULL, buf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
  if(img_ptr == MAP_FAILED){
    fprintf(stderr, "ERROR: mmap failed.\n");
    exit(1);
  }

  sb = (struct superblock *)(img_ptr + BSIZE); // superblock start

  dip = (struct dinode *)(img_ptr + IBLOCK((uint)0)*BSIZE);  // inode block start

  bitmap = (char *)(img_ptr + BBLOCK(0,sb->ninodes)*BSIZE);  // bitmap block start

  check_bad_inode();  // Bad inode check
  check_bad_direct_and_indirect();  // Bad direct and indirect address check

  dir = (struct dirent *)(img_ptr + (dip[ROOTINO].addrs[0])*BSIZE);

  check_root_dir_exist(); // Root directory exists check
  check_more_link_dir();  // More links in directory check

  traverse_bit_map(); // Bitmap generation

  return 0;
}
