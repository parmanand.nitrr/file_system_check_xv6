#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <assert.h>
#include <stdbool.h>

#include "types.h"
#include "fs.h"

#define BLOCK_SIZE (BSIZE)


int
main(int argc, char *argv[])
{
  int r,i,n,fsfd,r_fstat;
  char *addr;
  struct dinode *dip;
  struct superblock *sb;
  struct dirent *de;

  if(argc < 2){
    fprintf(stderr, "Usage: sample fs.img ...\n");
    exit(1);
  }



  fsfd = open(argv[1], O_RDONLY);
  if(fsfd < 0){
    perror(argv[1]);
    exit(1);
  }

  /* read information about fs.img */
  struct stat fstatb;
  if (stat(argv[1], &fstatb) == -1) {
        perror("stat");
        exit(EXIT_FAILURE);
    }

  /* Dont hard code the size of file. Use fstat to get the size */
  addr = mmap(NULL, (long)fstatb.st_size, PROT_READ, MAP_PRIVATE, fsfd, 0);
  if (addr == MAP_FAILED){
	perror("mmap failed");
	exit(1);
  }
  /* read the super block */
  sb = (struct superblock *) (addr + 1 * BLOCK_SIZE);
  printf("fs size %d, no. of blocks %d, no. of inodes %d \n", sb->size, sb->nblocks, sb->ninodes);

  /* read the inodes */
  dip = (struct dinode *) (addr + IBLOCK((uint)0)*BLOCK_SIZE); 
  printf("begin addr %p, begin inode %p , offset %ld \n", addr, dip, (char *)dip-addr);
  printf("Root inode  size %d links %d type %d \n", dip[ROOTINO].size, dip[ROOTINO].nlink, dip[ROOTINO].type);


  /* read all inodes*/
  printf("*******************************Question 1 Start*************************************************");   printf("\n");
  int inodenum=0;
  dip = (struct dinode *) (addr + IBLOCK((uint)inodenum)*BLOCK_SIZE);
  for(i = 1;i<sb->ninodes+1;i++){
  dip++;
  // printf("inode num %d type %d \n",i, dip->type);
  }   
  printf("******************************Question1 End*****************************************************");
  printf("\n");


  // get the address of root dir 
 de = (struct dirent *) (addr + (dip[ROOTINO].addrs[0])*BLOCK_SIZE);

  // print the entries in the first block of root dir 
 n = dip[ROOTINO].size/sizeof(struct dirent);
  for (i = 0; i < n; i++,de++){
	printf(" inum %d, name %s \n", de->inum, de->name);
  	printf("inode  size %d links %d type %d \n", dip[de->inum].size, dip[de->inum].nlink, dip[de->inum].type);
  }
  exit(0);

}

